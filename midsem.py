# ===================
# Mid Sem Exam
# ===================
# - Please copy the file called `midsem.py` into your **OWN** folder.
# - From now on, everything must be done in your own copy of the file.
# - Edit the file to solve the problem stated in it.
# - Save it, then commit your code and push to your fork of the project.
# - Make sure the tests are passing. If tests fail, you can look at the tests and figure out what you did wrong.
# - Submit a merge request to the Master branch of the class project.
# - Make sure that rebase issues do not exist or give rebase permissions to reviewer while creating the merge request
# - Once you are sure you are done and want to submit, simply mention @theSage21 in the comments of the merge request.
# - The timestamp on this comment shows when you finished your exam. You must finish within the exam time limit.

# NOTE: If at any time you have questions, please open an issue in the class project

# ===================
# Statement
# ===================

# You must write a function that returns a location in a game of tic-tac-toe
# your agent must try to win as many games as possible
# The board is a tuple of 3 strings. For example the starting position is:
#    board[] =  {'X', 'X', 'O', 
                   'O', 'O', 'X',
                   'X', 'O', 'X'};
# If your agent decides to put your symbol in coordinates (0, 0) the board would look like:

#     board[]=('0','X','X',
                 'X','X','X',
                 'X','X','0')
# Your agent must return two coordinates whenever it is called
# for example in order to make the move shown above your function
# must be something like this:

# def agent(board, your_symbol):
#     return 0, 0


# ===================
# Scoring
# ===================

# The scoring is simple.
# For every game you win, you get 1 point
# For everything else you get 0 point (lose a game/end game due to invalid move)
# There are two test cases for this problem
# Each test case has a single opponent algorithm your agent must try to win against.
# You can see the jobs in the merge request to see how your submission is doing


def win_check(arr, char): 
    matches = [[0, 1, 2], [3, 4, 5], 
               [6, 7, 8], [0, 3, 6], 
               [1, 4, 7], [2, 5, 8], 
               [0, 4, 8], [2, 4, 6]] 
  
    for i in range(8): 
        if(arr[matches[i][0]] == char and
            arr[matches[i][1]] == char and
            arr[matches[i][2]] == char): 
            return True
    return False
  def is_valid(arr): 
    xcount = arr.count('X') 
    ocount = arr.count('O') 
      
   
    if(xcount == ocount+1 or xcount == ocount): 
    
        if win_check(arr, 'O'): 
         
            if win_check(arr, 'X'): 
                return "Invalid"
  
         
            if xcount == ocount: 
                return "Valid"
  
          
        if win_check(arr, 'X') and xcount != ocount+1: 
            return "Invalid"
                  if not win_check(arr, 'O'): 
            return "Valid"
              return "Invalid"
              
              
              arr = ['X', 'X', 'O', 
       'O', 'O', 'X',  
       'X', 'O', 'X'] 
print("Given board is " + is_valid(arr)) 
    
    
    
    
    
 