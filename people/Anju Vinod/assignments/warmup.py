# This assignment is to familiarize you with how the course will be conducted.
# The general steps are:
#
# 1. Copy an assignment file from this folder to the `people/<your name>` folder
# 2. Edit the `people/<your name>/<assignment number>.py` file and write your solution in it
# 3. Create a merge request to submit
# 4. Ensure that
#       - tests pass for your solution in the merge request
#       - you rebase or merge the project master into your merge request whenever possible
#
#
#
# For this warmup exercise please complete the `solution` function.
# This function needs to return a list of the first N even numbers


# ========================================== put your solution here
def solution(n):
  list=[]
  for i in range(2*n):
     if i%2==0:
       list.append(i)
  return list

# ========================================== no need to edit below this line
if __name__ == "__main__":

    def reference(n):
        return [i for i in range(n) if i % 2 == 0]

    for n in [0, 1, 10, 100, 1000]:
        assert list(solution(n)) == list(reference(n)), f"Your solution fails for n={n}"
